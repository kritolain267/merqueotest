# Merqueo Test

## Requerimientos técnicos para construir el proyecto

* Se requiere android 4.4+
* Gradle Version 3.3.2
* kotlin version 1.3.21
* minSdkVersion 19

* Librerias implementadas

	implementation 'com.squareup.retrofit2:retrofit:2.3.0'
    implementation 'com.squareup.retrofit2:converter-gson:2.3.0'
    implementation "com.squareup.retrofit2:adapter-rxjava2:2.3.0"
    implementation 'io.reactivex.rxjava2:rxandroid:2.0.1'

   	/* Indicator */
    implementation 'cc.cloudist.acplibrary:library:1.2.1'

## Capas de aplicación

** Capa de presentación **

Todo lo referente a los XML y vista de usuario.

** Capa de negocio **

Se utilizo el patron MVVM para el desarrollo de esta capa.

** Capa de datos **

Se utilizo retrofit para la gestion de llamados al Api

## ScreenShots

![1.png](1.png)
![2.png](2.png)
![3.png](3.png)
![4.png](4.png)
![5.png](5.png)
![6.png](6.png)

## Versioning

1.0.0

## Authors

* ** Carolain Lenes Beltran

## License

Carolain Lenes
