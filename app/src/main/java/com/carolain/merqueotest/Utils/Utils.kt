package com.carolain.merqueotest.Utils

import android.database.Cursor
import android.support.design.widget.Snackbar
import android.view.View

class Utils {

    /* Snackbar */
    fun showMessage(view: View, message: String){
        Snackbar.make(view, message, Snackbar.LENGTH_SHORT).setAction("Action", null).show()
    }

    /* Convert Cursor to ArrayList */


}

fun <T> Cursor.toList(block: (Cursor) -> T) : List<T> {
    return mutableListOf<T>().also { list ->
        if (moveToFirst()) {
            do {
                list.add(block.invoke(this))
            } while (moveToNext())
        }
    }
}