package com.carolain.merqueotest.Utils

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import com.carolain.merqueotest.Model.MoviesResult

class DBOpenHelper (context: Context, factory: SQLiteDatabase.CursorFactory?) :
                    SQLiteOpenHelper(context, DATABASE_NAME,
                    factory, DATABASE_VERSION){

    companion object {
        private val DATABASE_VERSION = 1
        private val DATABASE_NAME = "Products_Merqueo.db"
        val TABLE_NAME = "Products_table"
        val ID = "id"
        val POSTERPATH = "poster_path"
        val RELEASEDATE = "release_date"
        val TITLE = "title"
        val VOTEAVERAGE = "vote_average"
        val QUANTITY = "quantity"
        val CATEGORY = "category"
    }


    override fun onCreate(db: SQLiteDatabase) {
        val CREATE_PRODUCTS_TABLE = ("CREATE TABLE " +
                TABLE_NAME + "(" + ID + " INTEGER PRIMARY KEY," + POSTERPATH + " TEXT," + RELEASEDATE + " TEXT," + TITLE + " TEXT," +
                VOTEAVERAGE + " TEXT," + QUANTITY + " TEXT," + CATEGORY + " TEXT" + ")")
        db.execSQL(CREATE_PRODUCTS_TABLE)
    }

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME)
        onCreate(db)
    }

    fun insertData(moviesResult: MoviesResult, quantity: String, category: String) {
        val db = this.writableDatabase
        val values = ContentValues()
        values.put(ID, moviesResult.id)
        values.put(TITLE, moviesResult.title)
        values.put(POSTERPATH, moviesResult.posterPath)
        values.put(RELEASEDATE, moviesResult.releaseDate)
        values.put(TITLE, moviesResult.title)
        values.put(VOTEAVERAGE, moviesResult.voteAverage)
        values.put(QUANTITY, quantity)
        values.put(CATEGORY, category)
        db.insert(TABLE_NAME, null, values)
        db.close()
    }


    fun getAllData(): MutableList<MoviesResult> {
        val db = this.writableDatabase
        val cursor: Cursor
        val list = ArrayList<MoviesResult>()
        cursor = db.rawQuery("SELECT * FROM $TABLE_NAME", null)
        if (cursor != null) {
            if (cursor.count > 0) {
                cursor.moveToFirst()
                do {
                    val movieID = cursor.getInt(cursor.getColumnIndex(ID))
                    val moviePosterPath = cursor.getString(cursor.getColumnIndex(POSTERPATH))
                    val movieReleaseDate = cursor.getString(cursor.getColumnIndex(RELEASEDATE))
                    val movieTitle = cursor.getString(cursor.getColumnIndex(TITLE))
                    val movieVoteAverage = cursor.getString(cursor.getColumnIndex(VOTEAVERAGE))
                    val user = MoviesResult()
                    user.id = movieID
                    user.posterPath = moviePosterPath
                    user.releaseDate = movieReleaseDate
                    user.title = movieTitle
                    user.voteAverage = movieVoteAverage.toDouble()
                    list.add(user)
                } while (cursor.moveToNext())
            }
        }
        db.close()
        return list
    }

    fun getData(id: String ) : Int {
        val db = this.writableDatabase
        var productCount = 0
        val cursor: Cursor
        cursor = db.rawQuery("SELECT * FROM $TABLE_NAME WHERE id = '"+id +"'",null,null)
        //cursor = db.rawQuery("SELECT * FROM $TABLE_NAME WHERE id = 429617",null,null)
            if (cursor != null) {
            if (cursor.count > 0) {
                if (cursor.moveToFirst()){
                    productCount = cursor.count
                }

            }
        }
        db.close()
        return productCount
    }

    fun countData(): Int{
        val db = this.writableDatabase
        val cursor: Cursor
        cursor = db.rawQuery("SELECT COUNT(*) AS quantity FROM $TABLE_NAME", null)
        var productCount = 0
        if (cursor != null) {
            if (cursor.count > 0) {
                if (cursor.moveToFirst()){
                    productCount = cursor.getInt(cursor.getColumnIndex(QUANTITY))
                }
            }
        }
        db.close()
        return productCount
    }

    fun deleteData(id: String ) : Int? {
        val db = this.writableDatabase
        return db.delete(TABLE_NAME, "id =? ", arrayOf(id))
        db.close()
    }

    fun deleteAllData() : Int{
        val db = this.writableDatabase
        return db.delete(TABLE_NAME,"1",null)
        db.close()
    }
}