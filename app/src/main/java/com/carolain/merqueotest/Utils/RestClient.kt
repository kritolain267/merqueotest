package com.carolain.merqueotest.Utils

import com.carolain.merqueotest.ApiRest.ApiRest
import com.carolain.merqueotest.Model.MovieDetail.DetailMovieVO
import com.carolain.merqueotest.Model.MoviesResult
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

class RestClient {

    private var disposable: Disposable? = null
    private val apiRest by lazy {
        ApiRest.create()
    }


    // Method GEt Movies

    fun getMovies(category: String, page: String, moviesCompletion: (moviesData: MutableList<MoviesResult>?, success: Boolean) -> Unit) {
        val apiKey = Globals().apiKey
        val language = Globals().language
        disposable = apiRest.getMoviesByCategoriy(category, apiKey, language, page)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { result ->
                    run {
                        if (result.results!!.size > 0){
                            moviesCompletion.invoke(result.results,true)
                        }else{
                            moviesCompletion.invoke(null,false)
                        }
                    }
                },
                { error ->
                    run {
                        moviesCompletion.invoke(null, false)
                    }
                }
            )
    }

    // Method Get Search

    fun getSearch(query:String, page :String, searchCompletion: (searchData: MutableList<MoviesResult>?, success: Boolean) -> Unit) {
        val apiKey = Globals().apiKey
        val language = Globals().language
        disposable = apiRest.getSearch(apiKey, language, query, page)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { result ->
                    run {
                        if (result!! != null){
                            searchCompletion.invoke(result.results,true)
                        }else{
                            searchCompletion.invoke(null,false)
                        }
                    }
                },
                { error ->
                    run {
                        searchCompletion.invoke(null, false)
                    }
                }
            )
    }

    // Method Get Detail Movie

    fun getMovieDetail(idMovie: String, movieDetileCompletion: (movieDetailData: DetailMovieVO?, success: Boolean) -> Unit) {
        val apiKey = Globals().apiKey
        val language = Globals().language
        disposable = apiRest.getMovieDetail(idMovie, apiKey, language)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { result ->
                    run {
                        if (result!! != null){
                            movieDetileCompletion.invoke(result,true)
                        }else{
                            movieDetileCompletion.invoke(null,false)
                        }
                    }
                },
                { error ->
                    run {
                        movieDetileCompletion.invoke(null, false)
                    }
                }
            )
    }
}