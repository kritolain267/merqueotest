package com.carolain.merqueotest.Adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.TextView
import com.carolain.merqueotest.Model.MoviesResult
import com.carolain.merqueotest.ViewModel.MoviesViewModel
import com.squareup.picasso.Picasso
import com.carolain.merqueotest.R
import com.carolain.merqueotest.Utils.DBOpenHelper
import com.carolain.merqueotest.ViewModel.ShoppingCartViewModel

class AdapterMovies (parent : MoviesViewModel?, parenthoppingCart: ShoppingCartViewModel? ,private val list: List<MoviesResult>) : RecyclerView.Adapter<MovieViewHolder>() {

    interface OnBluetoothDeviceClickedListener {
        fun onBluetoothDeviceClicked(deviceAddress: String)
    }

    internal var indexRow: Int = 0
    internal var parentAdapter: MoviesViewModel? = parent
    internal var parentAdapterCart: ShoppingCartViewModel? = parenthoppingCart

    // numberOgfItems
    override fun getItemCount(): Int {
        return list.count()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieViewHolder {
        val layoutInflater = LayoutInflater.from(parent?.context)
        return MovieViewHolder(layoutInflater,parent)
    }

    override fun onBindViewHolder(holder: MovieViewHolder, position: Int) {
        val moviesObject: MoviesResult = list[position]
        holder.bind(moviesObject)
        holder.itemView.setOnClickListener {
            indexRow = holder.adapterPosition

            if(parentAdapter !=null){
                parentAdapter!!.openMoviesDetail(indexRow)
            }else{
                parentAdapterCart!!.openMoviesDetail(indexRow)
            }
        }
        // Add products
        holder.addButtonCard!!.setOnClickListener {
            indexRow = holder.adapterPosition

            if(parentAdapter !=null){
                parentAdapter!!.insertData(indexRow)
                holder.addButtonCard!!.visibility = View.GONE
                holder.deleteButtonCard!!.visibility = View.VISIBLE
            }else{

            }
        }

        holder.deleteButtonCard!!.setOnClickListener {
            indexRow = holder.adapterPosition
            if(parentAdapter !=null){
                parentAdapter!!.deleteData(moviesObject.id!!)
                holder.addButtonCard!!.visibility = View.VISIBLE
                holder.deleteButtonCard!!.visibility = View.GONE
            }else{
                parentAdapterCart!!.deleteData(moviesObject.id!!,indexRow)
            }
        }
    }
}

class MovieViewHolder(inflater: LayoutInflater, parent: ViewGroup) :
    RecyclerView.ViewHolder(inflater.inflate(R.layout.card_recycler_movie, parent, false)) {
    private var dateText: TextView? = null
    private var movieTitleText: TextView? = null
    private var calificationText: TextView? = null
    private var movieImage: ImageView? = null
    // Add products
    var addButtonCard: Button? = null
    var showQuantityText: TextView? = null
    var deleteButtonCard: Button? = null
    var db: DBOpenHelper? = null



    init {
        dateText = itemView.findViewById(R.id.dateText)
        movieTitleText = itemView.findViewById(R.id.movieTitleText)
        calificationText = itemView.findViewById(R.id.calificationText)
        movieImage = itemView.findViewById(R.id.movieImage)
        // Add products
        addButtonCard = itemView.findViewById(R.id.addButtonCard)
        showQuantityText = itemView.findViewById(R.id.showQuantityText)
        deleteButtonCard = itemView.findViewById(R.id.deleteButtonCard)
        db = DBOpenHelper(parent.context, null)


    }

    fun bind(moviesObject: MoviesResult) {
        dateText?.text = moviesObject.releaseDate
        movieTitleText?.text = moviesObject.title
        calificationText?.text = moviesObject.voteAverage.toString()


        val image = moviesObject.posterPath

        if (image == "-") {

            movieImage?.setImageResource(R.drawable.navigation_empty_icon)

        } else {
            //Picasso.with(this.parent).load(foto).into(holder.fotoBiciAmigo);
            Picasso.get().load("https://image.tmdb.org/t/p/w200" + image).into(movieImage);
        }

        if (db != null){
            val productExists = db!!.getData(moviesObject.id.toString())
            if (productExists > 0){
                deleteButtonCard?.visibility = View.VISIBLE
                addButtonCard?.visibility = View.GONE
            }else{
                deleteButtonCard?.visibility = View.GONE
                addButtonCard?.visibility = View.VISIBLE
            }
        }
    }

}