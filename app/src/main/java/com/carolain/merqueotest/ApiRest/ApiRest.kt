package com.carolain.merqueotest.ApiRest

import com.carolain.merqueotest.Model.MovieDetail.DetailMovieVO
import com.carolain.merqueotest.Model.MoviesVO
import com.carolain.merqueotest.Utils.Globals
import com.google.gson.GsonBuilder
import io.reactivex.Observable
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface ApiRest {

    @GET("3/movie/{category}")
    fun getMoviesByCategoriy(
        @Path("category") category: String, @Query("api_key") apiKey: String, @Query("language") language: String, @Query("page") page: String): Observable<MoviesVO>

    @GET("3/search/movie")
    fun getSearch(
        @Query("api_key") api_key: String, @Query("language") language: String, @Query("query") query: String, @Query("page") page: String): Observable<MoviesVO>

    @GET("3/movie/{movie_id}")
    fun getMovieDetail(
        @Path("movie_id") movie_id: String, @Query("api_key") apiKey: String, @Query("language") language: String): Observable<DetailMovieVO>


    companion object {
        fun create(): ApiRest {

            val gson = GsonBuilder()
                .setLenient()
                .create()

            val retrofit = Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                //.baseUrl("http://suplitapi.inkubo.co/v1/")
                .baseUrl(Globals().url)
                .build()

            return retrofit.create(ApiRest::class.java)
        }
    }
}