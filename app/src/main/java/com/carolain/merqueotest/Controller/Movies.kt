package com.carolain.merqueotest.Controller

import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.content.pm.ActivityInfo
import android.graphics.Color
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.ImageButton
import android.widget.TextView
import cc.cloudist.acplibrary.ACProgressConstant
import cc.cloudist.acplibrary.ACProgressFlower
import com.carolain.merqueotest.R
import com.carolain.merqueotest.Utils.DBOpenHelper
import com.carolain.merqueotest.Utils.RecyclerViewPage
import com.carolain.merqueotest.Utils.Utils
import com.carolain.merqueotest.ViewModel.MoviesViewModel
import kotlinx.android.synthetic.main.activity_movies.*
import java.util.logging.Handler
import java.util.logging.LogRecord

class Movies : AppCompatActivity() {

    lateinit var icon_shopping_cart: ImageButton

    lateinit var recyclerViewMovies: RecyclerView
    private lateinit var moviesViewModel: MoviesViewModel
    private var lastPosition = 0
    private var page = 1
    var category = "popular"

    lateinit var dialog : Dialog
    internal var doubleBackToExitPressedOnce = false

    //DBOPpenHelper
    var db: DBOpenHelper? = null
    lateinit var showQuantityText: TextView

    override fun onResume() {
        super.onResume()
        if (db != null){
            updateShoppingCartValue()
        }

        if (recyclerViewMovies.adapter != null){
            recyclerViewMovies.adapter!!.notifyDataSetChanged()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_movies)
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        configurerView()
    }

    fun configurerView() {

        moviesViewModel = MoviesViewModel(this)
        icon_shopping_cart = findViewById(R.id.icon_shopping_cart) as ImageButton
        recyclerViewMovies = findViewById(R.id.recyclerViewMovies) as RecyclerView
        recyclerViewMovies.layoutManager = LinearLayoutManager(this)
        recyclerViewMovies.adapter
        recyclerViewMovies.addOnScrollListener(recyclerViewPagerOnScrollListener);
        showQuantityText = findViewById(R.id.showQuantityText) as TextView

        searchEditext.setOnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                var editSearch = searchEditext.text.toString()
                moviesViewModel.getSearchModel( editSearch, page.toString(),true)
                val imm = this.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                imm.hideSoftInputFromWindow(searchEditext.windowToken, 0)
            }
            true
        }


        /* -- Show Dialog--- */
        dialog = ACProgressFlower.Builder(this)
            .direction(ACProgressConstant.DIRECT_CLOCKWISE)
            .themeColor(Color.WHITE)
            .text("")
            .fadeColor(Color.DKGRAY).build()

        moviesViewModel.getMoviesModel(category, page.toString(),false)

        /* -- DBOPpenHelper -- */
        db = DBOpenHelper(this, null)
        showQuantityText = findViewById(R.id.showQuantityText) as TextView

        updateShoppingCartValue()

    }

    private fun updateShoppingCartValue(){
        val quantity = db!!.countData()

        if(quantity > 0){
            showQuantityText.visibility = View.VISIBLE
            showQuantityText.text = quantity.toString()
        }else{
            showQuantityText.visibility = View.GONE
            showQuantityText.text = "0"
        }
    }

    /* Menu */

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle presses on the action bar menu items
        page = 1
        when (item.itemId) {

            R.id.action_popular -> {
                category = "popular"
                moviesViewModel.getMoviesModel(category, page.toString(),true)
                return true
            }
            R.id.action_top_rated -> {
                category = "top_rated"
                moviesViewModel.getMoviesModel(category, page.toString(),true)
                return true
            }
            R.id.action_upcoming -> {
                category = "upcoming"
                moviesViewModel.getMoviesModel(category, page.toString(),true)
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    /* RecyclerView OnScrollListener*/
    private val recyclerViewPagerOnScrollListener = object : RecyclerView.OnScrollListener() {
        override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
            super.onScrollStateChanged(recyclerView, newState)
        }

        override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
            super.onScrolled(recyclerView, dx, dy)
            val recyclerViewPage = RecyclerViewPage.createHelper(recyclerView)
            val visibleItemCount = recyclerView!!.childCount
            val firstVisibleItem = recyclerViewPage.findFirstVisibleItemPosition()
            if (moviesViewModel.list!!.size != null && moviesViewModel.list!!.isNotEmpty()) {
                var lastItem = firstVisibleItem + visibleItemCount

                if (lastItem ==  moviesViewModel.list!!.size) {
                    lastPosition = lastItem
                    page += 1
                    moviesViewModel.getMoviesModel(category, page.toString(),false)
                }
            }
        }
    }

    /*----------- Buttons Methods -----------*/

    fun screenShoppingCart(ImageButton: View) {

        val intent = Intent(this,ShoppingCart::class.java)
        startActivity(intent)
    }

    // DOUBLE CLICK BUTTON EXIT BACK APP

    override fun onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed()
            val intent = Intent(Intent.ACTION_MAIN)
            intent.addCategory(Intent.CATEGORY_HOME)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            startActivity(intent)
            android.os.Process.killProcess(android.os.Process.myPid())
            return
        }

        this.doubleBackToExitPressedOnce = true
        val message = resources.getString(R.string.goOut)
        Utils().showMessage(recyclerViewMovies, message)

        object : Handler() {
            fun postDelayed(runnable: Runnable, i: Int) {}

            override fun publish(record: LogRecord) {

            }

            override fun flush() {

            }

            @Throws(SecurityException::class)
            override fun close() {

            }
        }.postDelayed(Runnable { doubleBackToExitPressedOnce = false }, 2000)
    }

}