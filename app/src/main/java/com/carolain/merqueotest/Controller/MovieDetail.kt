package com.carolain.merqueotest.Controller

import android.app.Dialog
import android.content.pm.ActivityInfo
import android.graphics.Color
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.webkit.WebView
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import cc.cloudist.acplibrary.ACProgressConstant
import cc.cloudist.acplibrary.ACProgressFlower
import com.carolain.merqueotest.R
import com.carolain.merqueotest.Utils.DBOpenHelper
import com.carolain.merqueotest.ViewModel.MovieDetailViewModel

class MovieDetail : AppCompatActivity() {
    private lateinit var movieDetailViewModel: MovieDetailViewModel

    lateinit var movieDetailImage: ImageView
    lateinit var videoWebView: WebView
    lateinit var titleText: TextView
    lateinit var languageText: TextView
    lateinit var dateDetailText: TextView
    lateinit var voteCountText: TextView
    lateinit var voteAverageText: TextView
    lateinit var homePageText: TextView
    lateinit var descriptionText: TextView
    lateinit var addButtonDetail: Button
    lateinit var deleteButtonDetail: Button

    lateinit var dialog : Dialog

    //DBOPpenHelper
    var db: DBOpenHelper? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_movie_detail)
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        supportActionBar?.hide()
        var movieId : String?
        val extras = intent.extras
        if (extras != null) {
            movieId = extras.getString("movieId") as String
            configurerView(movieId)
        }
    }

    fun configurerView(movieId : String){
        db = DBOpenHelper(this, null)
        movieDetailViewModel = MovieDetailViewModel(this)

        movieDetailImage = findViewById(R.id.movieDetailImage) as ImageView
        videoWebView = findViewById(R.id.videoWebView) as WebView
        titleText = findViewById(R.id.titleText) as TextView
        languageText = findViewById(R.id.languageText) as TextView
        dateDetailText = findViewById(R.id.dateDetailText) as TextView
        voteCountText =  findViewById(R.id.voteCountText) as TextView
        voteAverageText =  findViewById(R.id.voteAverageText) as TextView
        homePageText =  findViewById(R.id.homePageText) as TextView
        descriptionText = findViewById(R.id.descriptionText) as TextView
        addButtonDetail = findViewById(R.id.addButtonDetail) as Button
        deleteButtonDetail = findViewById(R.id.deleteButtonDetail) as Button


        /* Dialog */
        dialog = ACProgressFlower.Builder(this)
            .direction(ACProgressConstant.DIRECT_CLOCKWISE)
            .themeColor(Color.WHITE)
            .text("")
            .fadeColor(Color.DKGRAY).build()

        movieDetailViewModel.getMovieDetailModel(movieId)
    }

    /*----------- Buttons Methods -----------*/

    fun goBack(ImageButton: View) {

        this.finish()
    }

    fun addProduct(button: View){
        movieDetailViewModel.insertData()
    }

    fun  deleteProduct(button: View){
        movieDetailViewModel.deleteData()
    }

}
