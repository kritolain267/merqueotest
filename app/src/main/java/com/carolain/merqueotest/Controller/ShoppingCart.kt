package com.carolain.merqueotest.Controller

import android.app.Dialog
import android.content.pm.ActivityInfo
import android.graphics.Color
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.RelativeLayout
import cc.cloudist.acplibrary.ACProgressConstant
import cc.cloudist.acplibrary.ACProgressFlower
import com.carolain.merqueotest.R
import com.carolain.merqueotest.Utils.DBOpenHelper
import com.carolain.merqueotest.ViewModel.ShoppingCartViewModel

class ShoppingCart : AppCompatActivity() {

    private lateinit var shoppingCartViewModel: ShoppingCartViewModel
    lateinit var recyclerViewMovies: RecyclerView
    lateinit var dialog : Dialog

    // View emtpy Cart
    lateinit var relativeViewEmptyCart: RelativeLayout

    //DBOPpenHelper
    var db: DBOpenHelper? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_shopping_cart)
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        supportActionBar?.hide()
        configurerView()
    }

    fun configurerView() {

        shoppingCartViewModel = ShoppingCartViewModel(this)
        recyclerViewMovies = findViewById(R.id.recyclerViewMovies) as RecyclerView
        recyclerViewMovies.layoutManager = LinearLayoutManager(this)
        recyclerViewMovies.adapter

        /* -- View emtpy Cart --- */
        relativeViewEmptyCart = findViewById(R.id.relativeViewEmptyCart) as RelativeLayout

        /* -- Show Dialog--- */
        dialog = ACProgressFlower.Builder(this)
            .direction(ACProgressConstant.DIRECT_CLOCKWISE)
            .themeColor(Color.WHITE)
            .text("")
            .fadeColor(Color.DKGRAY).build()

        /* -- DBOPpenHelper -- */
        db = DBOpenHelper(this, null)
        shoppingCartViewModel.getAllData()

    }


    /*----------- Buttons Methods -----------*/

    fun closeScreen(ImageButton: View) {

        this.finish()
    }

    /*----------- Buttons Methods -----------*/

    fun deleteData(ImageButton: View) {

        shoppingCartViewModel.deleteAllData()
    }

    fun addNewProduct(ImageButton: View){
        shoppingCartViewModel.addNewProduct()
    }

}
