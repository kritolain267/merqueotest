package com.carolain.merqueotest.ViewModel

import android.content.Intent
import android.view.View
import com.carolain.merqueotest.Adapter.AdapterMovies
import com.carolain.merqueotest.Controller.MovieDetail
import com.carolain.merqueotest.Controller.ShoppingCart
import com.carolain.merqueotest.Model.MoviesResult

class ShoppingCartViewModel (private var shoppingCart : ShoppingCart) {
    val shoppingCartController = shoppingCart
    internal var list: MutableList<MoviesResult>? = null



    // GetAllData DataBase
    fun getAllData(){
        val listProducts = shoppingCartController.db!!.getAllData()
        succesMoviesResult(listProducts)
    }


    // Function Movies Result
    fun succesMoviesResult(listMovies : MutableList<MoviesResult>){
        if (listMovies!!.size != 0) {
            list = listMovies
            val adapterMovies = AdapterMovies(null, this ,list!!)
            shoppingCartController.recyclerViewMovies.setAdapter(adapterMovies)
        }else{
            shoppingCartController.relativeViewEmptyCart.visibility = View.VISIBLE
        }
    }

    fun openMoviesDetail(index: Int){
        val idMovie = list!!.get(index).id.toString()
        val intent = Intent(shoppingCartController, MovieDetail::class.java)
        intent.putExtra("movieId",idMovie)
        shoppingCartController.startActivity(intent)
    }
     // DeleteData DataBase

    fun deleteAllData(){
        val delete = shoppingCartController.db!!.deleteAllData()
        if (delete > 0){
            shoppingCartController.relativeViewEmptyCart.visibility = View.VISIBLE
        }
    }

    fun addNewProduct(){
        shoppingCartController.finish()
    }

    fun deleteData(id : Int, index : Int){
        shoppingCartController.db!!.deleteData(id.toString())
        list!!.removeAt(index)
        shoppingCartController.recyclerViewMovies.adapter!!.notifyDataSetChanged()
        val quantity = shoppingCartController.db!!.countData()

        if(quantity == 0){
            shoppingCartController.relativeViewEmptyCart.visibility = View.VISIBLE
        }
    }
}



