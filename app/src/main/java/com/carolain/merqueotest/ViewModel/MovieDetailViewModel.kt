package com.carolain.merqueotest.ViewModel

import android.support.design.widget.Snackbar
import android.view.View
import com.carolain.merqueotest.Controller.MovieDetail
import com.carolain.merqueotest.Model.MovieDetail.DetailMovieVO
import com.carolain.merqueotest.Model.MoviesResult
import com.carolain.merqueotest.Utils.RestClient
import com.squareup.picasso.Picasso
import java.text.ParseException
import java.text.SimpleDateFormat
import com.carolain.merqueotest.R
import kotlinx.android.synthetic.main.activity_movie_detail.*
import java.util.*

class MovieDetailViewModel(private var movieDetail : MovieDetail) {
    val movieDetailController  = movieDetail
    var detailMovieVO : DetailMovieVO? = null

    fun getMovieDetailModel(idMovie: String) {
        movieDetailController.dialog.show()
        RestClient().getMovieDetail(idMovie) { movieDetail, success ->
            if (success) {

                if (movieDetail != null) {
                    detailMovieVO = movieDetail
                    updateUIDetail(movieDetail)
                } else {
                    Snackbar.make(
                        movieDetailController.movieDetailImage, // Parent view
                        ("no fount"), // Message to show
                        Snackbar.LENGTH_SHORT // How long to display the message.
                    ).show()
                }

            } else {


            }
            movieDetailController.dialog.hide()

        }

    }

    fun updateUIDetail(detailMovie: DetailMovieVO){

        movieDetailController.titleText?.text = detailMovie.originalTitle
        movieDetailController.languageText?.text = detailMovie.originalLanguage
        movieDetailController.dateDetailText.setText(formateDateFromstring("yyyy-MM-dd","dd, MMM yyyy",detailMovie.releaseDate!!))
        movieDetailController.voteCountText?.text = detailMovie.voteCount.toString()
        movieDetailController.voteAverageText?.text = detailMovie.voteAverage.toString()
        movieDetailController.homePageText?.text = detailMovie.homepage
        movieDetailController.descriptionText?.text = detailMovie.overview

        val image = detailMovie.backdropPath

        if (image == "-") {

            movieDetailController.movieDetailImage?.setImageResource(R.drawable.ic_launcher_background)

        } else {
            Picasso.get().load("https://image.tmdb.org/t/p/w200" + image).into(movieDetailController.movieDetailImage);
        }

        val imagePoster = detailMovie.posterPath

        if (imagePoster == "-") {

            movieDetailController.movieDetailImage?.setImageResource(R.drawable.ic_launcher_background)

        } else {
            Picasso.get().load("https://image.tmdb.org/t/p/w200" + image).into(movieDetailController.smallImage);
        }

        val productExists = movieDetailController.db!!.getData(detailMovie.id.toString())
        if (productExists > 0){
            movieDetailController.deleteButtonDetail?.visibility = View.VISIBLE
            movieDetailController.addButtonDetail?.visibility = View.GONE
        }else{
            movieDetailController.deleteButtonDetail?.visibility = View.GONE
            movieDetailController.addButtonDetail?.visibility = View.VISIBLE
        }

    }

    // Insert products dataBase

    fun insertData(){
        val moviesResult = MoviesResult()
        moviesResult.id = detailMovieVO!!.id
        moviesResult.title = detailMovieVO!!.title
        moviesResult.posterPath = detailMovieVO!!.posterPath
        moviesResult.releaseDate = detailMovieVO!!.releaseDate
        moviesResult.title = detailMovieVO!!.title
        moviesResult.voteAverage = detailMovieVO!!.voteAverage

        val quantity = "1"
        val category = ""
        movieDetailController.db!!.insertData(moviesResult, quantity, category)
        movieDetailController.addButtonDetail.visibility = View.GONE
        movieDetailController.deleteButtonDetail.visibility = View.VISIBLE
    }

    fun deleteData(){
        movieDetailController.db!!.deleteData(detailMovieVO!!.id.toString())
        movieDetailController.addButtonDetail.visibility = View.VISIBLE
        movieDetailController.deleteButtonDetail.visibility = View.GONE
    }


    // Date format

    fun formateDateFromstring(inputFormat: String, outputFormat: String, inputDate: String): String {

        var parsed: Date? = null
        var outputDate = ""

        val df_input = SimpleDateFormat(inputFormat, Locale.getDefault())
        val df_output = SimpleDateFormat(outputFormat, Locale.getDefault())

        try {
            parsed = df_input.parse(inputDate)
            outputDate = df_output.format(parsed)

        } catch (e: ParseException) {
        }

        return outputDate

    }
}