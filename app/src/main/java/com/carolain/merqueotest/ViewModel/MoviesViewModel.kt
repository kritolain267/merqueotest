package com.carolain.merqueotest.ViewModel

import android.content.Intent
import android.support.design.widget.Snackbar
import android.view.View
import com.carolain.merqueotest.Adapter.AdapterMovies
import com.carolain.merqueotest.Controller.MovieDetail
import com.carolain.merqueotest.Controller.Movies
import com.carolain.merqueotest.Model.MoviesResult
import com.carolain.merqueotest.Utils.RestClient

class MoviesViewModel (private var movies : Movies)  {


    val moviesController  = movies
    internal var list: MutableList<MoviesResult>? = null


    // Movies Request

    fun getMoviesModel(category: String, page: String, changeCategory: Boolean) {
        RestClient().getMovies(category, page) { listMovies, success ->
            if (success) {
                succesMoviesResult(listMovies!!,changeCategory)
            } else {


            }

        }

    }

    fun openMoviesDetail(index: Int){
        val idMovie = list!!.get(index).id.toString()
        val intent = Intent(moviesController,MovieDetail::class.java)
        intent.putExtra("movieId",idMovie)
        moviesController.startActivity(intent)
    }

    // Function succes get Movies
    fun succesMoviesResult(listMovies : MutableList<MoviesResult>, changeCategory: Boolean){
        if (listMovies!!.size != 0) {
            if (changeCategory == true){
                list!!.clear()
                moviesController.recyclerViewMovies.adapter!!.notifyDataSetChanged()
                list!!.addAll(listMovies)
                moviesController.recyclerViewMovies.adapter!!.notifyDataSetChanged()
            }else{
                if (list == null){
                    list = listMovies
                    val adapterMovies = AdapterMovies(this, null, list!!)
                    moviesController.recyclerViewMovies.setAdapter(adapterMovies)
                }else{
                    list!!.addAll(listMovies)
                    moviesController.recyclerViewMovies.adapter!!.notifyDataSetChanged()
                }
            }
        } else {
            Snackbar.make(
                moviesController.recyclerViewMovies, // Parent view
                ("no fount"), // Message to show
                Snackbar.LENGTH_SHORT // How long to display the message.
            ).show()
        }
    }

    // Search Request

    fun getSearchModel(query:String, page :String, changeCategory: Boolean) {
        RestClient().getSearch(query, page) { listMovies, success ->
            if (success) {
                succesMoviesResult(listMovies!!,changeCategory)
            }

        }
    }

    // Insert products dataBase

    fun insertData(index: Int){
        val moviesResult = list!!.get(index)
        val quantity = "1"
        val category = moviesController.category
        moviesController.db!!.insertData(moviesResult, quantity, category)


        val text = moviesController.showQuantityText.text.toString().toInt()
        val showQuantity = text + 1
        moviesController.showQuantityText.text = showQuantity.toString()
        moviesController.showQuantityText.visibility = View.VISIBLE
    }

    fun deleteData(id : Int){
        moviesController.db!!.deleteData(id.toString())
        val text = moviesController.showQuantityText.text.toString().toInt()
        val showQuantity = text - 1
        if (showQuantity == 0){
            moviesController.showQuantityText.visibility = View.GONE
        }
        moviesController.showQuantityText.text = showQuantity.toString()
    }


}